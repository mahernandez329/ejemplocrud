@extends('Layouts.app')
@section('content')
@include('navbar')
<section class="container" >
    <div class="row">
        <article class="col-md-10 col-md-offset-1">
            {!!Form::open(['route'=>'movie.store','method'=>'post','novalidate'])!!}
            <div class="form-group" >
                <label>Nombre</label>
                <input type="text" name="name" class="form-control" required >
            </div>
            <div class="form-group">
                <label>Descripcion</label>
                <input type="text" name="description" class="form-control" required >
            </div>
            <div class="form-group">
                <label>Categorias</label>
                 
            </div>
            <div class="form-group">
            <select name="listCategories[]" class="form-control">
                <option value="">--Escoja la categoria</option>
                @foreach ($category as $categories)
                <option value="{{$categories->id}}">{{ $categories->name}}</option>
                @endforeach
            </select>
            
        </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success" > Enviar </button>
            </div>
            {!!Form::close() !!}
        </article>
    </div>
</section>
@endsection