@extends('layouts.app')
@section('content')
@include('navbar')
<section class="container">
	<div class="row">
		<article class="col-md-12">
			{!! Form::open(['route' => 'state/show', 'method' => 'post', 'class' => 'form-inline']) !!}
			<div class="form-gorup">
 				<input type="text" class="form-control" name="name">
			</div>
			<div class="">
				<button type="submit" class="btn btn-default">Search</button>
				<a href="{{ route('state.index') }}" class="btn btn-primary">All</a>
                <a href="{{ route('state.create') }}" class="btn btn-primary">Create</a>
			</div>
			{!! Form::close() !!}
		</article>
		<article class="col-md-12">
			<table class="table table-condensed table-striped table-bordered">
				<thead>
					<tr>
						<th>Estado</th>
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($states as $state)
					<tr>
						<td>{{ $state->state}}</td>
						
						<td>
							<a class="btn btn-primary btn-xs" href="{{ route('state.edit',['id' => $state->id]) }}">Edit</a>
							 <a class="btn btn-danger btn-xs" href="{{ route('state/destroy',['id' => $state->id]) }}">Delete</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
		</article>
	</div>
</section>
@endsection