@extends('layouts.app')
@section('content')
@include('navbar')
<section class="container">
	<div class="row">
		<article class="col-md-12">
			{!! Form::open(['route' => 'user/show', 'method' => 'post', 'class' => 'form-inline']) !!}
			<div class="form-gorup">
 				<input type="text" class="form-control" name="name">
			</div>
			<div class="">
				<button type="submit" class="btn btn-default">Search</button>
				<a href="{{ route('user.index') }}" class="btn btn-primary">All</a>
                <a href="{{ route('user.create') }}" class="btn btn-primary">Create</a>
			</div>
			{!! Form::close() !!}
		</article>
		<article class="col-md-12">
			<table class="table table-condensed table-striped table-bordered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Email</th>
						
						<th>Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{ $user->name}}</td>
						<td>{{ $user->email}}</td>
						
						
						<td>
							<a class="btn btn-primary btn-xs" href="{{ route('user.edit',['id' => $user->id]) }}">Edit</a>
							 <a class="btn btn-danger btn-xs" href="{{ route('user/destroy',['id' => $user->id]) }}">Delete</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			
		</article>
	</div>
</section>
@endsection