@extends('Layouts.app')
@section('content')
@include('navbar')
<section class="container" >
    <div class="row">
        <article class="col-md-10 col-md-offset-1">
            {!!Form::model($category,['route'=>['category.update',$category->id],'method'=>'put','novalidate'])!!}
            <div class="form-group" >
                <label>name</label>
                <input type="text" name="name" class="form-control" required value="{{$category->name}}" >
            </div>
            <div class="form-group">
                <label>Estado</label>
                <select name="state_id"class="form-control">
                <option value="">--Estado--</option>
                @foreach ($states as $state)
                <option value="{{$state->id}}">{{ $state->state}}</option>
                @endforeach
            </select>
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-success" > Enviar </button>
            </div>
            {!!Form::close() !!}
        </article>
    </div>
</section>
@endsection