@extends('layouts.app')
@section('content')
@include('navbar')
<section class="container">
	<div class="row">
		<article class="col-md-12">
			{!! Form::open(['route' => 'category/show', 'method' => 'post', 'class' => 'form-inline']) !!}
			<div class="form-gorup">
				 
				<input type="text" class="form-control" name="name">
			</div>
			<div class=" ">
				<button type="submit" class="btn btn-default">Search</button>
				<a href="{{ route('category.index') }}" class="btn btn-primary">All</a>
                <a href="{{ route('category.create') }}" class="btn btn-primary">Create</a>
			</div>
			{!! Form::close() !!}
		</article>
		<article class="col-md-12">
			<table class="table table-condensed table-striped table-bordered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Estado</th>
						<th>Opciones</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{ $category->name}}</td>
						<td>{{ $category->state->state}}</td>
						<td>
							<a class="btn btn-primary btn-xs" href="{{ route('category.edit',['id' => $category->id]) }}">Edit</a>
							 <a class="btn btn-danger btn-xs" href="{{ route('category/destroy',['id' => $category->id]) }}">Delete</a>
					@endforeach
				</tbody>
			</table>
			
		</article>
	</div>
</section>
@endsection