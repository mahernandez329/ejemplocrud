<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Models\Movie;

class MovieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
    {
        $faker= Faker::create();
        for ($i=0; $i < 10 ; $i++) { 
        	$movie= new Movie;
        	$movie->name=$faker->name;
        	$movie->description=$faker->name;
        	$movie->user_id=1;
        	$movie->state_id=1;
        	$movie->save();
        }
    }
}
