<?php

use Illuminate\Database\Seeder;

//class faker genera los datos aleatorios
use Faker\Factory as Faker;
//class user para crerar nuevos usuarios
use App\User;
//facade para generar passwords encriptados
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 10; $i++) { 
        	$user = new User;
        	$user->name = $faker->name;
        	$user->email = $faker->email;
        	$user->password = Hash::make('test123');
        	$user->state_id = 1;
        	$user->save();

        	
        }
    }
}
