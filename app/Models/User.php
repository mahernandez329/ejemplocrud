<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
   
    protected  $tabla='users';
    protected $fillable = ['name','email','password'];
    protected $guarded = ['id'];
    
}
