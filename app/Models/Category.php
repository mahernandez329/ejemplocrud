<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
       protected $table = 'categories';
    protected $fillable = ['name'];
    protected $guarded = ['id'];

    public function movies(){
    	return $this->belongsToMany('App\Models\Movie');
    }
    public function state(){
    	return $this->belongsTo('App\Models\State');
    }
}
