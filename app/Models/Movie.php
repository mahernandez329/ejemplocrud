<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
   
    protected  $tabla='movies';
    protected $fillable = ['name', 'description'];
    protected $guarded = ['id'];
    
    public function categories(){
    	return $this->belongsToMany('App\Models\Category');
    }
    public function state(){
    	return $this->belongsTo('App\Models\State');
    }
    public function user(){
    	return $this->belongsTo('App\User');
    }

}




