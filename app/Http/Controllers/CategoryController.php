<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category as Category;
use App\Models\State as State;

class categoryController extends Controller
{
	 public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->state_id = 1;
        $category->save();
        return redirect('category');   
    }
    public function create()
    {
        return \View::make('categories/new');
    }
    public function index()
    {
        $categories = Category::all();
        return \View::make('categories/list',compact('categories'));
    }
    
     public function edit($id) {
        $category = Category::find($id);
        $states = State::all();
        return \View::make('categories/update', compact('category','states'));
    }

    public function update($id, Request $request) {
        $category = Category::find($id);
        $category->name = $request->name;
         $category->state_id =$request->state_id;
        $category->save();
        return redirect('category');
    }

    public function show(Request $request) {
        $categories = Category::where('name', 'like', '%' . $request->name . '%')->get();
        return \View::make('categories/list', compact('categories'));
    }

    public function destroy($id) {
        $category = Category::find($id);
        $category->delete();
        return redirect()->back();
    }
   
}
