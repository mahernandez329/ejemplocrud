<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});
/*movies*/
Route::resource('movie','MovieController');
Route::get('movie/destroy/{id}',['as'=>'movie/destroy','uses'=>'MovieController@destroy']);
Route::post('movie/show',['as'=>'movie/show','uses'=>'Moviecontroller@show']);

/*categorias*/
Route::resource('category','categoryController');
Route::get('category/destroy/{id}',['as'=>'category/destroy','uses'=>'categoryController@destroy']);
Route::post('category/show',['as'=>'category/show','uses'=>'categorycontroller@show']);

/*usuarios*/
Route::resource('user','UserController');
Route::get('user/destroy/{id}',['as'=>'user/destroy','uses'=>'UserController@destroy']);
Route::post('user/show',['as'=>'user/show','uses'=>'Usercontroller@show']);
Auth::routes();

/*state*/
Route::resource('state','StateController');
Route::get('state/destroy/{id}',['as'=>'state/destroy','uses'=>'StateController@destroy']);
Route::post('state/show',['as'=>'state/show','uses'=>'Statecontroller@show']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
